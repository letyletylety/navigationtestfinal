package com.lety.testpopup


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.lety.testpopup.databinding.FragmentBlankBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class BlankFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding = DataBindingUtil.inflate<FragmentBlankBinding>(inflater, R.layout.fragment_blank, container, false)

        binding.aText.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_blankFragment_to_blankFragment2)
        }

        binding.gotocButton.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_blankFragment_to_blankFragment3)
        }

        return binding.root
    }

}
